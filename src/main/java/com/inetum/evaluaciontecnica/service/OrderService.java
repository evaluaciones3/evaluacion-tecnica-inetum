package com.inetum.evaluaciontecnica.service;
import com.inetum.evaluaciontecnica.entity.Order;
import com.inetum.evaluaciontecnica.exception.ResourceNotFoundException;
import com.inetum.evaluaciontecnica.repository.OrderRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {
    private final OrderRepository orderRepository;
    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }
    public Order createOrder(Order order) {
        return orderRepository.save(order);
    }
    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }
    public Order getOrderById(Long id) {
        return orderRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Orden no encotrada"));

    }
    public Order updateOrder(Long id, Order order) {
        Order existingOrder = getOrderById(id);
        if (existingOrder == null) {
           throw new ResourceNotFoundException("Orden no encontrada");
        }
        existingOrder.setId(id);
        existingOrder.setOrderDate(order.getOrderDate());
        existingOrder.setCustomerName(order.getCustomerName());

        return orderRepository.save(existingOrder);
    }

    public void deleteOrder(Long id) {

        orderRepository.deleteById(id);
    }
}