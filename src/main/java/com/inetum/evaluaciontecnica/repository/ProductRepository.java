package com.inetum.evaluaciontecnica.repository;
import com.inetum.evaluaciontecnica.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
}