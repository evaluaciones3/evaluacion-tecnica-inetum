package com.inetum.evaluaciontecnica.service;
import com.inetum.evaluaciontecnica.entity.Order;
import com.inetum.evaluaciontecnica.repository.OrderRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
@SpringBootTest
 public class OrderServiceTest {

    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private OrderService orderService;

    @Test
    public void testGetOrderById() {
        Order order = new Order(1L, "Order 1", new Date());
        Mockito.when(orderRepository.findById(1L)).thenReturn(Optional.of(order));
        assertEquals(order, orderService.getOrderById(1L));
    }

    @Test
    public void testGetAllOrders() {
        List<Order> orders = Arrays.asList(
                new Order(1L, "Order 1", new Date()),
                new Order(2L, "Order 2", new Date())
        );
        Mockito.when(orderRepository.findAll()).thenReturn(orders);
        assertEquals(orders, orderService.getAllOrders());
    }

    @Test
    public void testSaveOrder() {
        Order order = new Order(1L, "Order 1", new Date());
        Mockito.when(orderRepository.save(order)).thenReturn(order);
        assertEquals(order, orderService.createOrder(order));
    }

    @Test
    public void testDeleteOrder() {
        orderService.deleteOrder(1L);
        Mockito.verify(orderRepository, Mockito.times(1)).deleteById(1L);
    }
}