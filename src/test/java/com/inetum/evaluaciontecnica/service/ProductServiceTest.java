package com.inetum.evaluaciontecnica.service;
import com.inetum.evaluaciontecnica.entity.Product;
import com.inetum.evaluaciontecnica.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
@SpringBootTest
 public class ProductServiceTest {

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private ProductService productService;

    @Test
    public void testGetProductById() {
        Product product = new Product(1L, "Product 1", 10.0);
        Mockito.when(productRepository.findById(1L)).thenReturn(Optional.of(product));
        assertEquals(product, productService.getProductById(1L));
    }

    @Test
    public void testGetAllProducts() {
        List<Product> products = Arrays.asList(
                new Product(1L, "Product 1", 10.0),
                new Product(2L, "Product 2", 20.0)
        );
        Mockito.when(productRepository.findAll()).thenReturn(products);
        assertEquals(products, productService.getAllProducts());
    }

    @Test
    public void testSaveProduct() {
        Product product = new Product(1L, "Product 1", 10.0);
        Mockito.when(productRepository.save(product)).thenReturn(product);
        assertEquals(product, productService.createProduct(product));
    }

    @Test
    public void testDeleteProduct() {
        productService.deleteProduct(1L);
        Mockito.verify(productRepository, Mockito.times(1)).deleteById(1L);
    }
}
