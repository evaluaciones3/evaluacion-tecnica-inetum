package com.inetum.evaluaciontecnica.evaluacion;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
public class EvaluacionProblema2Test {
    @Test
    public void problemaNumero2() {
        assertEquals(2, minAmigos(8, 1, 15, 1, 10));
        assertEquals(1, minAmigos(7, 1, 15, 1, 10));
        assertEquals(-1, minAmigos(11, 1, 15, 1, 10));
        assertEquals(1, minAmigos(10000000000L, 1, 100000000000L, 1, 10));
    }
    public static int minAmigos(long D, int M, long D1, int N, long D2) {

//        validamos que D sea menor igual a 10^18 y sea mayor o igual a uno, tambien validamos
//         que M sea mayor o igual a cero y menor a 100, tambien validamos que D1 sea mayor o igual a uno
//         y no sea mayor a 10^18  y que N sea mayor o igual a cero y menor a 2*10^9 y que D2 sea mayor o igual a 1
//         y que sea menor a D1

        if (!(D >= 1 && D <= 1e18 && M >= 0 && M < 100 && D1 >= 1 && D1 <= 1e18 && N >= 0 && N < 2e9 && D2 >= 1 && D2 <= D1))
            return -1;

        // Ordenamos los compañeros por distancia máxima ascendente

        long[] amigos = new long[M + N];
        for (int i = 0; i < M; i++) {
            amigos[i] = D1;
        }
        for (int i = M; i < M + N; i++) {
            amigos[i] = D2;
        }
        Arrays.sort(amigos);
        int i = M + N - 1;
        int count = 0;
        while (D > 0 && i >= 0) {
            D = 2 * D - amigos[i];
            i--;
            count++;
        }
        if (D > 0) return -1;
        return count;
    }
}
