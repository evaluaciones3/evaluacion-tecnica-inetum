package com.inetum.evaluaciontecnica.evaluacion;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
public class EvaluacionProblema1Test {
    @Test
    public void problemaNumero1() {
        String[] matriz1 = {"N" };
        String[] matriz2 =
                {"NNYN",
                        "NNYN",
                        "NNNN",
                        "NYYN" };

        assertEquals(1, calcularSalarios(matriz1));
        assertEquals(5, calcularSalarios(matriz2));

    }
    public int calcularSalarios(String[] matriz) {

//        validamos que la matriz no sea nula y que la matriz tenga al menos un elemento
//         y tambien validamos que la matriz sea cuadrada
        if (matriz == null || matriz.length == 0 || matriz.length != matriz[0].length())
            return -1;
        int n = matriz.length;
        int[] salarios = new int[n];
        boolean[] recorridos = new boolean[n];
        for (int i = 0; i < n; i++) {
            if (!recorridos[i]) {
                dfs(i, matriz, salarios, recorridos);
            }
        }
        int salarioTotal = 0;
        for (int i = 0; i < n; i++) {
            salarioTotal += salarios[i];
        }
        return salarioTotal;
    }
    private void dfs(int empleado, String[] matriz, int[] salarios, boolean[] recorridos) {
        recorridos[empleado] = true;
        salarios[empleado] = 1;
        for (int j = 0; j < matriz[0].length(); j++) {
            if (matriz[empleado].charAt(j) == 'Y' && !recorridos[j]) {
                dfs(j, matriz, salarios, recorridos);
                salarios[empleado] += salarios[j];
            }
        }
    }

}
